#
# arch linux intial install script
#
set -e  # exit immediately if any errors
function confirm() {
    read -p "$1 ([y]es or [N]o): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
}
function umountFS() {
    mp=${1}
    sd=${2:-"false"}
    if grep -q "[[:space:]]${mp}[[:space:]]" /proc/mounts; then
        echo "unmounting ${mp}"
        umount ${mp}
        sleep 1
    fi
}
echo "Verify you are booted in UEFI mode, is directory populated?"
ls /sys/firmware/efi/efivars
if [[ "no" == $(confirm "continue?") ]]; then exit 0; fi
sleep 1
loadkeys uk
sleep 1
echo "create wifi session file"
cat > /etc/netctl/wifi-session << EOF
Description='wifi auto connect profile'
Interface=wlo1
Connection=wireless
Security=wpa
ESSID=AZ4KAM63S4UDLWFZX7
IP=dhcp
Key=Let4Me7Not2To5The9
EOF
sleep 1
echo "start wifi session"
netctl start wifi-session
sleep 10
echo "test network connection"
ping -c5 google.co.uk
sleep 1
echo "set network timekeping on"
timedatectl set-ntp true
sleep 2
timedatectl status
sleep 1
echo "list of block storage devices"
lsblk /dev/sda
sleep 2
umountFS "/dev/sda1"
umountFS "/dev/sda2"
echo "format linux root on /dev/sda2"
mkfs.ext4 /dev/sda2
sleep 1
echo "format uefi partition on /dev/sda1"
mkfs.fat -F32 /dev/sda1
sleep 1
if [[ $(swapon -s | grep -ci "/dev" ) -gt 0 ]] ;  then
    echo "swapoff /dev/sda3"
    swapoff /dev/sda3
    sleep 1
fi
echo "format swap on /dev/sda3"
mkswap /dev/sda3
sleep 1
echo "mount swap on /dev/sda3"
swapon /dev/sda3
sleep 1
echo "mount root on /dev/sda2"
mount /dev/sda2 /mnt
sleep 1
echo "mount boot on /dev/sda1"
mkdir -p /mnt/boot
sleep 1
mount /dev/sda1 /mnt/boot
sleep 2
if [[ ! -e /etc/pacman.d/mirrorlist.original ]]; then
    echo "backup original mirrorlist"
    mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.original
    sleep 1
fi
echo "create UK mirror list"
cat > /etc/pacman.d/mirrorlist << EOF
##
## Arch Linux repository mirrorlist
## Sorted by mirror score from mirror status page
## Generated on 2016-02-17
##

## Score: 0.8, United Kingdom
Server = http://archlinux.mirrors.uk2.net/\$repo/os/\$arch
## Score: 0.8, United Kingdom
Server = http://mirror.bytemark.co.uk/archlinux/\$repo/os/\$arch
## Score: 1.0, United Kingdom
Server = http://mirrors.manchester.m247.com/arch-linux/\$repo/os/\$arch
## Score: 1.1, United Kingdom
Server = http://mirror.cinosure.com/archlinux/\$repo/os/\$arch
## Score: 3.8, United Kingdom
Server = http://www.mirrorservice.org/sites/ftp.archlinux.org/\$repo/os/\$arch
## Score: 4.6, United Kingdom
Server = http://arch.serverspace.co.uk/arch/\$repo/os/\$arch

EOF
sleep 1
echo "install base system"
pacstrap -i /mnt base base-devel
sleep 1
echo "update fstab"
genfstab -U /mnt >> /mnt/etc/fstab
sleep 2
cat /mnt/etc/fstab
sleep 1
