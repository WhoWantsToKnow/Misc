#!/bin/bash
# ---------------------------------------------------------------------------
# structure-files.sh - Move files in source directory into structured
#                      directories under a target root.
#
# Copyright 2014, Keith Watson<swillber@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at <http://www.gnu.org/licenses/> for
# more details.
#
# Usage: structure_files [-h|--help]
#
# Revision history:
# 2014-12-26 Created by new_script ver. 3.3
# ---------------------------------------------------------------------------
#
PROGNAME=${0##*/}
VERSION="0.1"
#
# ---------------------------------------------------------------------------
#
clean_up() { # Perform pre-exit housekeeping
    # restore $IFS
    IFS=$SAVEIFS
    return
}
#
# ---------------------------------------------------------------------------
#
error_exit() {
    echo -e "${PROGNAME}: ${1:-"Unknown Error"}" >&2
    clean_up
    exit 1
}
#
# ---------------------------------------------------------------------------
#
graceful_exit() {
    clean_up
    exit
}
#
# ---------------------------------------------------------------------------
#
signal_exit() { # Handle trapped signals
    case $1 in
        INT)
            error_exit "Program interrupted by user" ;;
        TERM)
            echo -e "\n$PROGNAME: Program terminated" >&2
            graceful_exit ;;
        *)
            error_exit "$PROGNAME: Terminating on unknown signal" ;;
    esac
}
#
# ---------------------------------------------------------------------------
#
usage() {
    echo -e "Usage: $PROGNAME [-h|--help]"
}
#
# ---------------------------------------------------------------------------
#
help_message() {
    cat <<- _EOF_
    $PROGNAME ver. $VERSION
    Move files in source directory into structured
    directories under a target root.

    $(usage)

    Options:
    -h, --help  Display this help message and exit.

_EOF_
  return
}
#
# ---------------------------------------------------------------------------
#
# Trap signals
    trap "signal_exit TERM" TERM HUP
    trap "signal_exit INT"  INT
#
# ---------------------------------------------------------------------------
#
# Parse command-line
    while [[ -n $1 ]]; do
        case $1 in
        -h | --help)
            help_message; graceful_exit ;;
        -* | --*)
            usage
            error_exit "Unknown option $1" ;;
        *)
            echo "Argument $1 to process..." ;;
        esac
        shift
    done
#
# ================================= MAIN LOGIC =================================
#
    # set some progress counters
    FC=0 # count all files processed
    DC=0 # count files copied to directory
    #
    # set source directory
    SRC=/home/swillber/MMedia/GPs
    #
    # set target root directory
    TGT=/home/swillber/MMedia/SD08-11N
    #
    # count files in source
    SC=$(ls -1 $SRC | wc -l)
    #
    # save field separator string
    SAVEIFS=$IFS
    #
    # remove space so it's acceptable in filenames
    IFS=$(echo -en "\n\b")
    #
    # process all files one at a time
    for T in $SRC/*
    do
        #
        # strip off path
        F=$(basename $T)
        #
        # get 1st character of filename
        F1="${F:0:1}"
        #
        # convert to uppercase
        F1=${F1^^}
        #
        # build target directory name
        D="$TGT/$F1"
        #
        # if target dir doesn't exist, create it
        # (assume change of 1st character of filename)
        if [ ! -d $D ]; then
            if [ $DC -gt 0 ]; then
                #
                # total no. files processed so far
                FC=$(( FC + DC ))
                #
                # calculate percentage completed
                PC=$(echo "scale=2; ($FC*100)/$SC" | bc)
                echo $(printf "%-20s" "files copied $DC")"Total: $FC of $SC ($PC%)."
                #
                # reset count of files in directory
                DC=0
            fi
            #
            # create new directory
            mkdir "$D"
            echo -n $(printf "%-13s" "Created:'$F1'")
        fi
        #
        # copy file to target and count
        cp $T $D
        ((++DC))
    done
    graceful_exit
